package com.itau.jogovelha.controller;

import com.itau.jogovelha.model.ConnexaoRabbit;
import com.itau.jogovelha.model.Jogada;
import com.itau.jogovelha.model.Placar;
import com.itau.jogovelha.model.Rodada;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.itau.jogovelha.model.Tabuleiro;

@Controller
@CrossOrigin
public class JogoController {
	int contaJogadas = 0;
	
	//@Autowired
	ConnexaoRabbit conexao = new ConnexaoRabbit();
	
	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");
	
	@RequestMapping("/")
	public @ResponseBody
	Placar getPlacar() {
		return rodada.getPlacar();
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) throws IOException {
        rodada.jogar(jogada.x, jogada.y);
        String msg = "Jogada "+ contaJogadas ++; 
        conexao.envia(msg);

        return rodada.getPlacar();
    }

    @RequestMapping("/iniciar")
    public @ResponseBody
    Placar iniciarJogo() {
        rodada.iniciarJogo();

        return rodada.getPlacar();
    }
}
