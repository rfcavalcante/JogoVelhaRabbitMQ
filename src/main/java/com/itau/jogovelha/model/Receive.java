package com.itau.jogovelha.model;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Receive {
	ConnectionFactory factory;
	Connection conn;
	Channel channel;
	Consumer consumer;

	public Receive() throws IOException, TimeoutException {
		factory = new ConnectionFactory();
		factory.setHost("localhost");
		conn = factory.newConnection();
		channel = conn.createChannel();
		channel.queueDeclare("mtech", false, false,	false, null);
		consumer= new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envolope, AMQP.BasicProperties properties, byte[] body ) throws IOException {
				String message = new String (body, "UTF-8"); 
				System.out.println("Receives" + message );

			}

		};

		channel.basicConsume("mtech", true, consumer);
	}
}
