package com.itau.jogovelha.model;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class ConnexaoRabbit {
	ConnectionFactory factory;
	Connection conn;
	Channel channel;

	public ConnexaoRabbit() {
		factory = new ConnectionFactory();
		factory.setHost("localhost");
		try {
			conn = factory.newConnection();
			channel = conn.createChannel();
			channel.queueDeclare("mtech", false, false,	false, null);
		} catch (IOException | TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void envia(String mensagem) throws IOException {


		channel.basicPublish("", "mtech", null, mensagem.getBytes());


	}

	public void fechaFila() throws IOException, Exception {
		channel.close();	
	}


	//	
	//	
	//	factory.setUri("amqp://userName:password@hostName:portNumber/virtualHost");
	//	Connection conn = factory.newConnection();
}
